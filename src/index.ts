import { html } from 'lit-html';
import { run, View, Update } from 'lit-tea';

type Anon = { tag: 'anon' }
type LoggingIn = { tag: 'logging-in' }
type LoggedIn = { tag: 'logged-in', token: string, name: string }

type AuthInfo =
  Anon
  | LoggingIn
  | LoggedIn

type Model = {
  auth: AuthInfo
}

type NoOp = { tag: 'noop' }

type Msg =
  NoOp

const init: Model = {
  auth: { tag: 'anon' }
};

const getUser = (ai: AuthInfo) => {
  switch (ai.tag) {
    case "anon":
      return "Anonymous";
    case "logging-in":
      return "...";
    case "logged-in":
      return ai.name;
  }
}

const view: View<Model, Msg> = currentModel =>
  html`
    <h1>Ahoy</h1>
    <h2>You are ${getUser(currentModel.auth)}</h2>
  `;

const update: Update<Model, Msg> = (currentModel, msg) => {
  switch (msg.tag) {
    case "noop":
      return currentModel;
  }
}

run({ init, view, update })
